#!/usr/bin/python3 

import json
import os
from sys import getsizeof

class UsbVendor:

    """Retrieves USB information from vendors ID + Name and Product ID + Name."""

    def __init__(self):

        """USBVendor class constructor."""

        # Vendor ID (VID)

        self.vendorID = []
        self.vendor = []

        # Product ID (PID)

        self.deviceID = []
        self.device = []

        path = os.getcwd() + "/Data/usb.json"

        with open(path, "r") as f:

            # Retrieves the json data in the form of a dictionary.
            data = json.load(f)

            # Runs all data in succession.
            for vendor in data:

                self.vendorID.append(vendor[:4])
                self.vendor.append(vendor[5:])

                for device in data[vendor]:

                    self.deviceID.append(device[:4])
                    self.device.append(device[5:])

    

    # .:: VendorID ::.

    def getVendorID(self):

        """Retrieves all seller IDs."""

        return self.vendorID



    def inVendorID(self, name):

        """Checks if a vendor ID is in the list."""

        if isinstance(name, str):
            if getsizeof(name) == 53:
                return True if name in self.vendorID else False
        
        return False



    def getVendorID2Vendor(self, name):

        """Returns the vendor name from the vendor ID."""

        if isinstance(name, str):
            if getsizeof(name) == 53:
                return list([self.vendor[line] for line in [index for index, value in enumerate(self.vendorID) if value == name]])

            
            
    # .:: Vendor ::.

    def getAllVendor(self):

        """Returns the list of vendors (id + name)."""

        return list(zip(self.vendorID, self.vendor))



    def getVendor(self):

        """Retrieve all the list of sellers."""

        return self.vendor

    

    def inVendor(self, name):

        """Checks if a seller is in the list."""

        if isinstance(name, str):
            return True if name in self.vendor else False
        
        return False

    

    # .:: DeviceID ::.

    def getDeviceID(self):

        """Retrieves all device IDs."""

        return self.deviceID

    
    
    def inDeviceID(self, name):

        """Checks if the device ID is in the list."""

        if isinstance(name, str):
            if getsizeof(name) == 53:
                return True if name in self.deviceID else False
        
        return False



    def getDeviceID2Device(self, name):

        """Returns the device name of the device ID device."""

        if isinstance(name, str):
            if getsizeof(name) == 53:
                return list([self.device[line] for line in [index for index, value in enumerate(self.deviceID) if value == name]])


    # .:: Device ::.

    def getAllDevice(self):

        """Returns the list of devices (ID + Name)."""

        return list(zip(self.deviceID, self.device))



    def getDevice(self):

        """Retrieves all device names."""

        return self.device 


    
    def inDevice(self, name):

        """Checks if the device name is in the list."""

        if isinstance(name, str):
            return True if name in self.device else False
        
        return False