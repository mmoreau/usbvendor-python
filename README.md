# USBVendor

**Identifies** USB data such as **"VendorID"**, **"VendorName"**, **"Product ID"**, **"Product Name"**.

## Dependencies

* Python 3 (https://www.python.org/downloads/)

## Source 

List of USB **Vendors** & **Devices** : www.linux-usb.org/usb.ids

## Example

**Linux** : #!/usr/bin/python3 


```python
    #!/usr/bin/python3 

    from Class.UsbVendor import UsbVendor
    from pprint import pprint

    usb = UsbVendor()
    
    pprint(usb.getDevice())
    pprint(usb.getDeviceID())
    pprint(usb.getVendor())
    pprint(usb.getVendorID())
    print(usb.getDeviceID2Device("00cc"))
```

## Contact

Feel free to contact me if you have any questions, criticisms, suggestions, bugs encountered 

* **Twitter :** https://twitter.com/mxmmoreau
* **LinkedIn :** https://www.linkedin.com/in/mxmoreau/